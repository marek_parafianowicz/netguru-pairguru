require 'rails_helper'

describe MovieDecorator do
	let(:genre) { create :genre, name: 'Sci-fi' }
	let(:movie) { create :movie, title: 'Primer', description: 'Sci-fi drama',
															 released_at: Date.parse('2004-10-08'), genre: genre }

	describe "#tmdb_cover" do
		subject { movie.decorate.tmdb_cover }
		it "Returns poster path from TMDB" do
			is_expected.to eq 'http://image.tmdb.org/t/p/w154//pYiAYDn3ltw9Fq7izODuq7oWYwX.jpg'
		end
	end

	describe "#tmdb_avg_rating" do
		subject { movie.decorate.tmdb_avg_rating }
		it "Returns average rating from TMDB" do
			is_expected.to eq 6.9
		end
	end

	describe "#tmdb_description" do
		subject { movie.decorate.tmdb_description }
		it "Returns movie description from TMDB" do
			is_expected.to include("Friends/fledgling entrepreneurs invent a device")
		end
	end
end