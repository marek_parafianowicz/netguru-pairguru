class MovieDecorator < Draper::Decorator
  delegate_all

  def cover
    'http://lorempixel.com/100/150/' + %W(abstract nightlife transport).sample + '?a=' + SecureRandom.uuid
  end

  def tmdb_cover
    tmdb_search(self)
    poster_url = @movie.poster_path
    'http://image.tmdb.org/t/p/w154/' + poster_url
  end

  def tmdb_avg_rating
    tmdb_search(self)
    @movie.vote_average
  end

  def tmdb_description
    tmdb_search(self)
    @movie.overview
  end

private
	
	def tmdb_search(obj)
    title = obj.title
    @movie = Tmdb::Search.movie(title).results.first
  end
end
 
