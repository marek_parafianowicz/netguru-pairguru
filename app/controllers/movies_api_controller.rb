class MoviesApiController < ApplicationController
  expose(:movies) { Movie.select("movies.id, movies.title") }
  expose(:movie) { Movie.find(params[:id])}

  def index
    render json: movies 
  end

  def show
    render json: movie
  end
end
